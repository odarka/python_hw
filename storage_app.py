from re import match
from datetime import date, timedelta
import json
from os import path


class Product:
    code = None
    name = None
    brand = None
    __price = None
    __quantity = None
    __date_act = None
    __action = None

    def __init__(self, code, name, brand, price, quantity, date_act, action):
        self.code = code
        self.name = name
        self.brand = brand
        self.price = price
        self.quantity = quantity
        self.date_act = date_act
        self.action = action

    @property
    def price(self):
        return self.__price

    @price.setter
    def price(self, value):
        if match(r'\d+(?:\.\d+)?', value) and value != '0':
            self.__price = float(value)
        else:
            raise Exception("incorrect price")

    @property
    def quantity(self):
        return self.__quantity

    @quantity.setter
    def quantity(self, value):
        if match(r'\d+(?:\.\d+)?', value):
            self.__quantity = float(value)
        else:
            raise Exception("incorrect quantity")

    @property
    def date_act(self):
        return self.__date_act

    @date_act.setter
    def date_act(self, value):
        if match(r'\b\d{4}-(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9]|3[0-1])\b', value) and \
                date(*map(int, value.split('-'))) <= date.today():
            self.__date_act = value
        else:
            raise Exception("incorrect date")

    @property
    def action(self):
        return self.__action

    @action.setter
    def action(self, value):
        if value == 'sell' or value == 'buy':
            self.__action = value
        else:
            raise Exception("incorrect action")

    def to_dict(self):
        return {
            'code': str(self.code),
            'name': str(self.name),
            'brand': str(self.brand),
            'price': str(self.price),
            'quantity': str(self.quantity),
            'date_act': str(self.date_act),
            'action': str(self.action)
        }


class ProductStorage:
    products = []
    FILE_NAME = 'data_file.json'

    def __init__(self):
        if path.exists(self.FILE_NAME):
            with open(self.FILE_NAME, 'r') as f:
                product_dicts = json.loads(f.read())['products']
            product_objects = []
            for product_dict in product_dicts:
                product_objects.append(Product(*map(str, product_dict.values())))

            self.products = product_objects

    def add_product(self, product):
        self.products.append(product)

    def report_mode(self, mode):
        if mode not in ['0', '7', '30']:
            raise Exception('incorrect mode')

        mode = int(mode)
        report = {}
        consumption = 0
        coming = 0
        profit = 0

        for product in self.products:
            if (date.today() - date(*map(int, product.date_act.split('-')))) <= timedelta(days=mode):
                if product.action == 'sell':
                    coming += (float(product.price)) * float(product.quantity)
                elif product.action == 'buy':
                    consumption += float(product.price) * float(product.quantity)

        profit += (coming - consumption)
        report['consumption'] = str(consumption)
        report['coming'] = str(coming)
        report['profit'] = str(profit)

        return report

    def store_to_file(self):
        json_value = json.dumps({'products': self.products}, default=lambda x: x.to_dict())

        with open(self.FILE_NAME, 'w') as write_file:
            write_file.write(json_value)


class App:

    def __init__(self):
        self.attributes = ['code', 'name', 'brand', 'price', 'quantity', 'date_act', 'action']
        self.store = ProductStorage()

    def run(self):
        while True:
            try:
                user_input = input('\nadd to add product enter, \'report\' to see report or \'ex\' to exit: ')
                if user_input == 'add':
                    self.__add_command()
                elif user_input == 'report':
                    self.__report_command()
                elif user_input == 'ex':
                    self.__exit_command()
                    return
            except Exception as ex:
                print(f'\nYou entered {ex}( try again!')

    def __add_command(self):
        product_input = ''

        for atr in self.attributes:
            atr_value = input(f"Enter a {atr}: ")
            product_input += atr_value

            if atr != self.attributes[-1]:
                product_input += ' '

        self.store.add_product(Product(*map(str, product_input.split(' '))))

    def __report_command(self):
        user_mode = input('\nEnter a mode 0 - day, 7 - week, 30 - month: ')
        print(f'\n{self.store.report_mode(user_mode)}')

    def __exit_command(self):
        self.store.store_to_file()


App().run()
