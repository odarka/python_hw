class Transport:
    def __init__(self, brand, model, type, max_volume_tank,
                 current_volume_tank, mileage, consumption):
        self.brand = brand
        self.model = model
        self.type = type
        self.max_volume_tank = max_volume_tank
        self.current_volume_tank = current_volume_tank
        self.mileage = mileage
        self.consumption = consumption

    def __str__(self):
        return f"brand: {self.brand}\n" \
               f"model: {self.model}\n" \
               f"class: {self.type}\n" \
               f"max volume of tank: {self.max_volume_tank}\n" \
               f"current volume of tank: {self.current_volume_tank}\n" \
               f"mileage: {self.mileage}\n" \
               f"consumption: {self.consumption}\n"

    def is_full_tank(self):
        return self.current_volume_tank == self.max_volume_tank

    def is_empty_tank(self):
        return self.current_volume_tank == 0

    def need_to_full_tank(self):
        return self.max_volume_tank - self.current_volume_tank

    def _fill_tank(self, litres):
        if self.need_to_full_tank() >= litres > 0:
            self.current_volume_tank += litres
            return "Fuel is filled\n"

        return "The wrong amount\n"

    def move(self, km):
        for km in range(km):
            if self.is_empty_tank():
                return "Tank is empty\n"
            self.current_volume_tank -= self.consumption
            self.mileage += 1

        return "You are arrived!\n"


class Car(Transport):
    def __init__(self, brand="BMW", model="X5", type="C", max_volume_tank=30,
                 current_volume_tank=30, mileage=0, consumption=1):
        super().__init__(brand=brand, model=model, type=type, max_volume_tank=max_volume_tank,
                         current_volume_tank=current_volume_tank, mileage=mileage, consumption=consumption)

    def __str__(self):
        return f"{super().__str__()}"

    def ride(self, km):
        return super().move(km)

    def fill_car_tank(self, litres):
        return super()._fill_tank(litres)


class Plane(Transport):
    def __init__(self, brand="Antonov", model="Mriya", type="super-heavy transport aircraft",
                 max_volume_tank=400, current_volume_tank=400, mileage=0, consumption=10):
        super().__init__(brand=brand, model=model, type=type, max_volume_tank=max_volume_tank,
                         current_volume_tank=current_volume_tank, mileage=mileage, consumption=consumption)

    def __str__(self):
        return f"{super().__str__()}"

    def fly(self, km):
        return super().move(km)

    def fill_plane_tank(self, litres):
        return super()._fill_tank(litres)


class Ship(Transport):
    def __init__(self, brand="White Star Line", model="Titanik", type="Olimpic",
                 max_volume_tank=2000, current_volume_tank=2000, mileage=0, consumption=10):
        super().__init__(brand=brand, model=model, type=type, max_volume_tank=max_volume_tank,
                         current_volume_tank=current_volume_tank, mileage=mileage, consumption=consumption)

    def __str__(self):
        return f"{super().__str__()}"

    def sail(self, km):
        return super().move(km)

    def fill_ship_tank(self, litres):
        return super()._fill_tank(litres)


class AmphibianMachine(Car, Ship):

    def __init__(self, brand="Amphibian", model="Amphibian Machine", type="M",
                 max_volume_tank=2000, current_volume_tank=2000, mileage=0, consumption=10):
        Transport.__init__(self, brand=brand, model=model, type=type, max_volume_tank=max_volume_tank,
                           current_volume_tank=current_volume_tank, mileage=mileage, consumption=consumption)

    def __str__(self):
        return Transport.__str__(self)

    def go(self, mode, km):
        if mode == "r":
            return super().ride(km)
        elif mode == "s":
            return super().sail(km)
        else:
            return f"unsupported mode\n"

    def fill_amphibian_tank(self, litres):
        return super()._fill_tank(litres)


my_car = Car()
my_car1 = Car("Opel", "z3", "B", 30, 30, 0, 9)
my_car2 = Car(brand="Audi", model="O4", type="A", max_volume_tank=50, current_volume_tank=40, mileage=9, consumption=3)
print(my_car)
print(my_car.ride(30))
print(my_car1.ride(200))
print(my_car2.ride(20))
print(my_car)
print(my_car.need_to_full_tank())
print(my_car.fill_car_tank(my_car.need_to_full_tank()))
print(my_car.is_full_tank())
print(my_car)
my_plane = Plane()
print(my_plane.fly(1000))
print(my_plane.fill_plane_tank(my_plane.need_to_full_tank()))
print(my_plane)
my_ship = Ship()
print(my_ship)
print(my_ship.sail(200))
print(my_ship.fill_ship_tank(my_ship.need_to_full_tank()))
my_amphibian_machine = AmphibianMachine()
print(my_amphibian_machine)
print(my_amphibian_machine.go('r', 10))
print(my_amphibian_machine.go('s', 20))
print(my_amphibian_machine)
print(my_amphibian_machine.fill_amphibian_tank(my_amphibian_machine.need_to_full_tank()))
print(my_amphibian_machine)
