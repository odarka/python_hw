def add_to_passed_cells(x, y, passed_cells):
    temp = [x, y]
    passed_cells.append(temp)

    return passed_cells


def check_way(enter_x, enter_y, x, y, labirint, passed_cells):
    if isEnd(enter_x, enter_y, x, y, labirint):
        return x, y
    if x >= 0 and labirint[x - 1][y] == "." and [x - 1, y] not in passed_cells:
        passed_cells = add_to_passed_cells(x - 1, y, passed_cells)
        res = check_way(enter_x, enter_y, x - 1, y, labirint, passed_cells)
        if res != 0:
            return res
    if x < len(labirint) and labirint[x + 1][y] == "." and [x + 1, y] not in passed_cells:
        passed_cells = add_to_passed_cells(x + 1, y, passed_cells)
        res = check_way(enter_x, enter_y, x + 1, y, labirint, passed_cells)
        if res != 0:
            return res
    if y >= 0 and labirint[x][y - 1] == "." and [x, y - 1] not in passed_cells:
        passed_cells = add_to_passed_cells(x, y - 1, passed_cells)
        res = check_way(enter_x, enter_y, x, y - 1, labirint, passed_cells)
        if res != 0:
            return res
    if y < len(labirint) and labirint[x][y + 1] == "." and [x, y + 1] not in passed_cells:
        passed_cells = add_to_passed_cells(x, y + 1, passed_cells)
        res = check_way(enter_x, enter_y, x, y + 1, labirint, passed_cells)
        if res != 0:
            return res
    return 0


def isEnd(enter_x, enter_y, x, y, labirint):
    if x == enter_x and y == enter_y:
        return False
    if labirint[x][y] == "." and (x == 0 or x == 9 or y == 0 or y == 9):
        return True
    else:
        return False


lb = [
    "##########",
    ".........#",
    "#.####.###",
    "#......###",
    "######.###",
    "#........#",
    "##.####.##",
    "#..##.#...",
    "#.......##",
    "#########",
]

passed_cels = add_to_passed_cells(1, 0, [])
finish = check_way(1, 0, 1, 0, lb, passed_cels)

# print(f"finish is {finish}")
