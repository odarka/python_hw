from datetime import datetime
import time
from re import match


# task 1


def run_time(fun):
    def wrapper_name(*args, **kwargs):
        start_time = datetime.now()
        value = fun(*args, **kwargs)
        print(f"time of running is {datetime.now() - start_time}")
        return value

    return wrapper_name


@run_time
def some_func():
    time.sleep(5)


# some_func()
# task 2


def count_down():
    user_number = input("Enter a number ")
    while match(r'^\d+$', user_number) is None:
        user_number = input("Enter a number ")
    user_number = int(user_number)
    list_num = []
    for item in range(user_number):
        list_num.insert(0, item * 3)
    print(list_num)


# count_down()
# task 3

def skip(result):
    def inner_wrapper_name(fun):
        # ACCEPT DECORATES FUNCTION VARS
        def wrapper_name():
            return result

        return wrapper_name

    return inner_wrapper_name


@skip("Does not work")
def func():
    print("I am working")


print(func())


# 4 task

def num_gen():
    n = 1
    while True:
        number = int(0.5 * n * (n + 1))
        n += 1
        yield number


number = num_gen()
# print(next(number))
# print(next(number))
# print(next(number))
