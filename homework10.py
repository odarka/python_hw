import json
import json


class ProductStorage:
    products = []

    @property
    def product(self, ):
        value = '\n'
        for product in self.products:
            value += f'{product}\n'
        return f"{value}\n"

    @product.setter
    def product(self, value):
        self.products.append({'name': value['name'],
                              'price': value['price']})

    @product.deleter
    def product(self):
        self.products = []

    def store_to_file(self):
        to_file = ''
        with open('test.txt', 'w') as f:
            for product in self.products:
                if self.products.index(product) != len(self.products) - 1:
                    to_file += f'{product["name"]} {product["price"]}\n'
                else:
                    to_file += f'{product["name"]} {product["price"]}'
            f.write(to_file)

    def load_from_file(self):
        with open('test.txt', 'r') as f:
            products_from_file = f.read().split('\n')
        for product in products_from_file:
            product_data = product.split(' ')
            product_object = {'name': product_data[0], 'price': product_data[1]}
            self.products.append(product_object)


storage = ProductStorage()
storage.load_from_file()
storage.product = {'name': 'fghjk', 'price': 123}
storage.product = {'name': 'cvbn', 'price': 12678}
storage.product = {'name': 'dfgyujm', 'price': 678}
storage.store_to_file()
# class ProductStorage:
#     products = []
#
#     def add_product(self, product):
#         with open('test.txt', 'a') as f:
#             f.write(str(product))
#
#     def read_products(self):
#         with open('test.txt', 'r') as f:
#             products = f.read().split('\n')
#         result = []
#         for product in products:
#             product_data = product.split(' ')
#             product_object = Product(product_data[0], product_data[1])
#             result.append(product_object)
#         return result

# product1 = Product('ghjk', 'hjk')
# product2 = Product('2', '2')
# product_storage = ProductStorage()
# product_storage.add_product(product1)
# product_storage.add_product(product2)
# print(product_storage.read_products()[0])

# xleb = Product('ghj', '123456')
# print(xleb.toJSON())
# with open("data_file.json", "w") as write_file:
#     write_file.write(xleb.toJSON())

# xleb = Product()
# with open('data_file.json', 'r') as f:
#     data = f.read()
#
# xleb.fromJSON(data)
# print(xleb.name)


# def toJSON(self):
#     return json.dumps(self, default=lambda o: o.__dict__,
#                       sort_keys=True, indent=4)
#
# def fromJSON(self, data):
#     self.__dict__ = json.loads(data)
