from operator import itemgetter


class HomeLibrary:
    __books = []

    @property
    def library(self):
        value = '\n'
        for book in self.__books:
            value += f'{book}\n'
        return f"{value}"

    @library.setter
    def library(self, value):
        self.__books.append({'name': value['name'],
                             'author': value['author'],
                             'year': value['year']})

    @library.deleter
    def library(self):
        self.__books = []

    def search(self, atr, search_str):
        match = []
        for book in self.__books:
            if book[atr] == search_str:
                match.append(book)
        return match

    def sort(self, atr):
        self.__books = sorted(self.__books, key=itemgetter(atr))


# lib = HomeLibrary()
# print(lib.library)
# lib.library = {'name': 'The Picture of Dorian Gray', 'author': 'Oscar Wills Wilde', 'year': 1878}
# lib.library = {'name': 'Winnie-the-Pooh', 'author': 'A. A. Milne', 'year': 1888}
# lib.library = {'name': 'The Chronicles of Narnia', 'author': ' Clive Staples Lewis', 'year': 1921}
# print(lib.library)
# print(lib.search('name', 'The Picture of Dorian Gray'))
# lib.sort("name")
# print(lib.library)

