from unittest import TestCase
from labirint import *
from homework9 import *


class TestLabirint(TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def test_add_to_passed_cells_3_4(self):
        test_cells = [3, 4]
        passed_cells = add_to_passed_cells(test_cells[0], test_cells[1], [])
        self.assertEqual(test_cells, passed_cells[-1])

    def test_check_way_labirint_without_exit(self):
        passed_cells = add_to_passed_cells(1, 0, [])
        res = check_way(1, 0, 1, 0, [
            "##########",
            ".........#",
            "#.####.###",
            "#......###",
            "######.###",
            "#........#",
            "##.####.##",
            "#..##.#..#",
            "#.......##",
            "#########",
        ], passed_cells)

        self.assertEqual(res, 0)

    def test_check_way_labirint_with_exit(self):
        passed_cells = add_to_passed_cells(1, 0, [])
        res = check_way(1, 0, 1, 0, [
            "##########",
            ".........#",
            "#.####.###",
            "#......###",
            "######.###",
            "#........#",
            "##.####.##",
            "#..##.#..#",
            "#.......##",
            "######.##",
        ], passed_cells)

        self.assertEqual(res, (9, 6))

    def test_isEnd_is_end(self):
        res = isEnd(1, 0, 9, 6, [
            "##########",
            ".........#",
            "#.####.###",
            "#......###",
            "######.###",
            "#........#",
            "##.####.##",
            "#..##.#..#",
            "#.......##",
            "######.##",
        ])

        self.assertEqual(res, True)

    def test_isEnd_not_end(self):
        res = isEnd(1, 0, 9, 7, [
            "##########",
            ".........#",
            "#.####.###",
            "#......###",
            "######.###",
            "#........#",
            "##.####.##",
            "#..##.#..#",
            "#.......##",
            "######.##",
        ])

        self.assertEqual(res, False)


class TestHomeLibrary(TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def test_book_setter_and_getter(self):
        test_book = {'name': 'The Picture of Dorian Gray', 'author': 'Oscar Wills Wilde', 'year': 1878}
        lib = HomeLibrary()
        lib.library = test_book
        test_str_book = '\n' + str(test_book) + '\n'
        self.assertEqual(test_str_book, lib.library)

    def test_book_deleter(self):
        lib = HomeLibrary()
        test_str = '\n'
        lib.library = {'name': 'The Picture of Dorian Gray', 'author': 'Oscar Wills Wilde', 'year': 1878}
        del lib.library
        self.assertEqual(test_str, lib.library)

    def test_search_by_name_success(self):
        lib = HomeLibrary()
        lib.library = {'name': 'The Picture of Dorian Gray', 'author': 'Oscar Wills Wilde', 'year': 1878}
        res = lib.search('name', 'The Picture of Dorian Gray')
        test_book = [{'author': 'Oscar Wills Wilde', 'name': 'The Picture of Dorian Gray', 'year': 1878}]
        self.assertEqual(test_book, res)

    def test_search_by_name_fail(self):
        lib = HomeLibrary()
        lib.library = {'name': 'The Picture of Dorian Gray', 'author': 'Oscar Wills Wilde', 'year': 1878}
        res = lib.search('name', 'The of Dorian Gray')
        test_book = []
        self.assertEqual(test_book, res)

    def test_search_by_author_success(self):
        lib = HomeLibrary()
        lib.library = {'name': 'The Picture of Dorian Gray', 'author': 'Oscar Wills Wilde', 'year': 1878}
        res = lib.search('author', 'Oscar Wills Wilde')
        test_book = [{'name': 'The Picture of Dorian Gray', 'author': 'Oscar Wills Wilde', 'year': 1878}]
        self.assertEqual(test_book, res)

    def test_search_by_author_fail(self):
        lib = HomeLibrary()
        lib.library = {'name': 'The Picture of Dorian Gray', 'author': 'Oscar Wills Wilde', 'year': 1878}
        res = lib.search('name', 'The of Dorian Gray')
        test_book = []
        self.assertEqual(test_book, res)

    def test_search_by_year_success(self):
        lib = HomeLibrary()
        lib.library = {'name': 'The Picture of Dorian Gray', 'author': 'Oscar Wills Wilde', 'year': 1878}
        res = lib.search('year', 1878)
        test_book = [{'author': 'Oscar Wills Wilde', 'name': 'The Picture of Dorian Gray', 'year': 1878}]
        self.assertEqual(test_book, res)

    def test_search_by_year_fail(self):
        lib = HomeLibrary()
        lib.library = {'name': 'The Picture of Dorian Gray', 'author': 'Oscar Wills Wilde', 'year': 1878}
        res = lib.search('name', 'The of Dorian Gray')
        test_book = []
        self.assertEqual(test_book, res)
