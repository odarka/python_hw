from re import findall, match
from datetime import datetime, time


# from time import strftime
# # # task 1
def write_file(file_location, data):
    with open(file_location, 'w') as f:
        f.write(data)


def open_read_file(file_location):
    with open(file_location, 'r') as f:
        data = f.read()
    return data


file_data = open_read_file("test.txt")
list_of_datas = findall(r'\b(?:0[1-9]|1[0-9]|2[0-9]|3[0-1])-(?:0[1-9]|1[0-2])-\d{4}\b', file_data)
print(list_of_datas)
# # task 2
user_year = input("Enter a year ")
while match(r'\b\d{4}\b', user_year) is None:
    user_year = input("Enter a year ")
user_year = int(user_year)
if user_year % 4 == 0 or (user_year % 100 != 0 and user_year % 400 == 0):
    print(f"{user_year} is intercalary year!")
else:
    print(f"{user_year} is not intercalary year!")
# task 3
file_data = open_read_file("test.txt")
lst_num = findall(r'-?\d+(?:\.\d+)?', file_data)
lst_num = [float(i) for i in lst_num]
print(lst_num)
print(sum(lst_num))
# task 4


def geom_gen(first, step):
    n = first
    while True:
        n *= step
        yield n


gen = geom_gen(2, 3)
print(next(gen))
print(next(gen))
print(next(gen))
# task 5
file_data = open_read_file("test.txt")
negative_numbers = findall(r'-\d+(?:\.\d+)?', file_data)
for negative_number in negative_numbers:
    file_data = file_data.replace(negative_number, "")
write_file("test.txt", file_data)
print(file_data)
# task 6
file_data = open_read_file("test.txt")
names_surnames_georaphic = findall(r'[A-Z]{1}[a-z]+', file_data)
print(names_surnames_georaphic)
