# task 3
alphabet = 'abcdefghijklmnopqrstuvwxyz'
numbers = '0123456789'


def read_file(file_location):
    with open(file_location, 'r') as f:
        file_data = f.read()
    return file_data


def write_file(file_location, file_data):
    with open(file_location, 'w') as f:
        f.write(file_data)


def replacing(file_data, step):
    decrypted = ""
    if len(alphabet) < abs(step):
        step_alpha = -(step % len(alphabet))
    else:
        step_alpha = -step
    if len(numbers) < abs(step):
        step_num = -(step % len(numbers))
    else:
        step_num = -step
    for char in file_data:
        if char and char in alphabet:
            try:
                decrypted += alphabet[alphabet.index(char) + step_alpha]
            except IndexError:
                decrypted += alphabet[step_alpha - (len(alphabet) - alphabet.index(char))]
        elif char.isupper() and char.lower() in alphabet:
            try:
                decrypted += alphabet[alphabet.index(char.lower()) + step_alpha].upper()
            except IndexError:
                decrypted += alphabet[step_alpha - (len(alphabet) - alphabet.index(char.lower()))].upper()
        elif char in numbers:
            try:
                decrypted += numbers[numbers.index(char) + step_num]
            except IndexError:
                decrypted += numbers[step_num - (len(numbers) - numbers.index(char))]
        else:
            decrypted += char
    return decrypted


def decryption(file_location, step):
    file_data = read_file(file_location)
    write_file(file_location, replacing(file_data, step))


decryption("input.txt", 7)
# task 2


def no_numbers(file_location):
    file_data = read_file(file_location)
    for number in range(10):
        file_data = file_data.replace(str(number), "")
    write_file(file_location, file_data)

# task 1


def if_null(variable):
    return variable == 0


def roman_num(arabic_num, one, five, ten):
    if if_null(arabic_num):
        return ""
    roman_number = ""
    arabic_num = int(arabic_num)
    if arabic_num <= 3:
        for item in range(arabic_num):
            roman_number += one
    elif 3 < arabic_num < 5:
        roman_number = five
        for item in range(5 - arabic_num):
            roman_number = one + roman_number
    elif 5 < arabic_num <= 8:
        roman_number = five
        for item in range(arabic_num - 5):
            roman_number = roman_number + one
    elif 8 < arabic_num < 10:
        roman_number = ten
        for item in range(10 - arabic_num):
            roman_number = one + roman_number
    elif arabic_num == 5:
        roman_number = five
    elif arabic_num == 10:
        roman_number = ten

    return roman_number


def roman_thousand(arabic_num):
    if if_null(arabic_num):
        return ""
    roman_number = ""
    if arabic_num <= 3:
        for item in range(arabic_num):
            roman_number += "M"
    elif arabic_num == 4:
        roman_number = "IX"
    else:
        roman_number = "X"

    return roman_number


def arabic_to_roman(arabic_number):
    res = ''

    if arabic_number > 5000:
        res = "it`s bigger then 5000"

    if arabic_number < 1:
        res = "it`s smaller then 1"

    if res == '':
        arabic_ones = arabic_number % 10
        arabic_dozens = arabic_number % 100 // 10
        arabic_hundreds = arabic_number % 1000 // 100
        arabic_thousands = arabic_number // 1000
        roman_ones = roman_num(arabic_ones, 'I', 'V', 'X')
        roman_dozens = roman_num(arabic_dozens, 'X', 'L', 'C')
        roman_hundreds = roman_num(arabic_hundreds, 'C', 'D', 'M')
        roman_thousands = roman_thousand(arabic_thousands)
        res = roman_thousands + roman_hundreds + roman_dozens + roman_ones

    return res


print(arabic_to_roman(888))
