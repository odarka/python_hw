from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from re import search
from kivy.properties import StringProperty
from kivy.uix.scrollview import ScrollView


class ScrollableLabel(ScrollView):
    text = StringProperty('')


class CalculatorApp(App):
    label = None
    text_but = '123+456-789*0.C/'
    index = 0
    user_input = ''
    answer = ''

    def build(self):
        layout = BoxLayout(orientation='vertical')
        row_layout = BoxLayout()
        self.label = ScrollableLabel(text='')
        row_layout.add_widget(self.label)
        layout.add_widget(row_layout)

        for button_row in range(4):
            row_layout = BoxLayout()
            for button_str in range(4):
                btn = Button(
                    text=f'{self.text_but[self.index]}',
                )

                btn.id = f'{self.text_but[self.index]}'
                btn.bind(on_press=self.on_press_button)

                self.index += 1
                row_layout.add_widget(btn)

            layout.add_widget(row_layout)

        btn_equal = Button(text='=')
        btn_equal.id = '='
        btn_equal.bind(on_press=self.on_press_button)
        layout.add_widget(btn_equal)
        return layout

    def on_press_button(self, instance: Button):
        try:
            if instance.id not in '+-/*C=':
                test = instance.id

                if not self.answer:
                    test = self.user_input + test

                matches = search(r'[\d.]*$', test)
                if matches and search(r'^\d+(?:\.\d*)?$', matches.group(0)) \
                        and not search(r'^([0]+\d+)$', matches.group(0)):
                    if self.answer:
                        self.answer = ''
                        self.user_input = instance.id
                        self.label.text = instance.id
                    else:
                        self.user_input = test
                        self.label.text = self.user_input
            elif instance.id in '-+*/' and self.label.text != '':
                test = self.user_input + instance.id
                if search(r'\d+[+\-*/]\d*$', test):
                    self.answer = ''
                    self.user_input = test
                    self.label.text = self.user_input
            elif instance.id == '=':
                self.answer = str(eval(self.user_input))
                self.label.text = self.answer
                self.user_input = self.answer
            elif instance.id == 'C':
                self.user_input = ''
                self.label.text = ''
                self.answer = ''

        except ZeroDivisionError:
            self.user_input = ''
            self.label.text = self.user_input
        except SyntaxError:
            self.user_input = ''
            self.label.text = self.user_input


if __name__ == '__main__':
    app = CalculatorApp()
    app.run()
